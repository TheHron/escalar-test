import React, { Component } from 'react'
import PropTypes from 'prop-types'

class UsersList extends Component {
  static propTypes = {
    users: PropTypes.array.isRequired,
  }

  render() {
    const { users } = this.props

    return (
      <div>
        {users.map(user =>
          <div className="row" key={user.id}>
            <div className="column">
              <p>{user.name}</p>
            </div>
            <div className="column">
              <p>{user.email}</p>
            </div>
            <div className="column">
              <p>{user.phone}</p>
            </div>
          </div>
        )}
      </div>
    )
  }
}

export default UsersList