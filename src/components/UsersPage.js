import React, { Component } from 'react'
import { 
  Link,
} from 'react-router-dom'
import { connect } from 'react-redux'
import UsersList from './UsersList'
import UsersTiles from './UsersTiles'

class UsersPage extends Component {
  linksState(props) {
    const { match: { params: { display } } } = this.props

    if (display === 'tiles') {
      return { listIsActive: false, tilesIsActive: true }
    } else {
      return { listIsActive: true, tilesIsActive: false }
    }
  }

  listLabel(listIsActive) {
    if (listIsActive) {
      return <span className="active">List</span>
    } else {
      return <Link exact to='/users/list'>List</Link>
    }
  }

  tilesLabel(tilesIsActive) {
    if (tilesIsActive) {
      return <span className="active">Tiles</span>
    } else {
      return <Link exact to='/users/tiles'>Tiles</Link>
    }
  }

  renderUsers() {
    const { match: { params: { display } }, users } = this.props

    if (display === 'tiles') {
      return <UsersTiles users={users}/>
    } else {
      return <UsersList users={users}/>
    }
  }

  render() {
    const { listIsActive, tilesIsActive } = this.linksState()

    return (
      <div className="users">
        <div className="users__header">
          {this.listLabel(listIsActive)}
          &nbsp;|&nbsp; 
          {this.tilesLabel(tilesIsActive)}
        </div>
        <div className="users__body">
          {this.renderUsers()}
        </div>
      </div>
    )
  }
}



function mapStateToProps(state) {
  return { users: state.users }
}

export default connect(mapStateToProps)(UsersPage)