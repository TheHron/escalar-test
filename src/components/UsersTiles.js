import React, { Component } from 'react'
import PropTypes from 'prop-types'


class UsersTiles extends Component {
  static propTypes = {
    users: PropTypes.array.isRequired,
  }

  render() {
    const { users } = this.props

    return (
      <div className="tiles">
        {users.map(user =>
          <div className="tiles__item" key={user.id}>
            <p>{user.name}</p>
            <p>{user.email}</p>
            <p>{user.phone}</p>
          </div>
        )}
      </div>
    )
  }
}

export default UsersTiles