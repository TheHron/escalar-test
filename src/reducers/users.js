const initialState = [
  {
    id: 1,
    name: 'Иванов Иван Иванович',
    phone: '+70000000000',
    email: 'ivanov@ivan.ru',
  },
  {
    id: 2,
    name: 'Петров Петр Петрович',
    phone: '+70000000001',
    email: 'petrov@petr.ru',
  },
  {
    id: 3,
    name: 'Сергеев Сергей Сергеевич',
    phone: '+70000000002',
    email: 'sergeev@sergei.ru',
  },
  {
    id: 4,
    name: '123Сергеев Сергей Сергеевич',
    phone: '+70000000002',
    email: 'sergeev@sergei.ru',
  }  
]

export default function users(state = initialState, action) {
  return state
}