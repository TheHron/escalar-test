import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import { createStore, applyMiddleware, compose } from 'redux'
import { Provider } from 'react-redux'
import createHistory from 'history/createBrowserHistory'
import { Route, Switch } from 'react-router'
import { ConnectedRouter, routerMiddleware } from 'react-router-redux'
import reducers from './reducers'
import UsersPage from './components/UsersPage'

const history = createHistory()
const middleware = routerMiddleware(history)

const store = createStore(
  reducers,
  applyMiddleware(middleware),
)

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <div className='layout'>
        <Switch>
          <Route exact path="/" component={UsersPage}/>
          <Route exact path="/users" component={UsersPage}/>
          <Route exact path="/users/:display" component={UsersPage}/>
        </Switch>
      </div>
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
)

registerServiceWorker();